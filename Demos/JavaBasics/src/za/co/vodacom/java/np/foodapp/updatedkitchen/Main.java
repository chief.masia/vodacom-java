package za.co.vodacom.java.np.foodapp.updatedkitchen;
public class Main {

  public static void main(String[] args) {
    FoodMeal pizzaMeal = new FoodMeal("Pizza", 20.3f);
    pizzaMeal.setNumberOfPersons(2);
    FoodMeal pastaMeal = new FoodMeal("Pasta",55.3f);
    pastaMeal.setNumberOfPersons(1);
    BevMeal orangeJuice = new BevMeal("Orange Juice", 10.2f);
    orangeJuice.setSizeInML(0.7f);

    System.out.println(pizzaMeal.getPrice());

    Menu mainMenu = new Menu("main");
    mainMenu.addMeal(pizzaMeal);
    mainMenu.addMeal(orangeJuice);
    mainMenu.addMeal(pastaMeal);

    //System.out.println(mainMenu.getCheapMeals());


    System.out.println(mainMenu);
  }
}
