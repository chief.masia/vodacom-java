package za.co.vodacom.java.np.foodapp.updatedkitchen;

public class FoodMeal extends Meal{

  private int numberOfPersons;
  public static final double serviceCharge = 0.15;



  public int getNumberOfPersons() {
    return numberOfPersons;
  }

  public void setNumberOfPersons(int numberOfPersons) {
    this.numberOfPersons = numberOfPersons;
  }
  public FoodMeal(String name, double price) {
    super(name, price);
  }

  @Override
  public String toString() {

    return "FoodMeal{" + super.toString() +
      "numberOfPersons=" + numberOfPersons +
      '}';
  }
}
