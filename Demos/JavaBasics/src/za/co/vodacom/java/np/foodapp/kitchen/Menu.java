package za.co.vodacom.java.np.foodapp.kitchen;

import java.util.Arrays;

public class Menu {

  private Meal[] allMeals;
  private String menuType;

  private int mealsCount = 0;
  public Menu(String menuType, int menuSize){
    this.menuType = menuType;
    this.allMeals = new Meal[menuSize];

  }

  public void addMealToMenu(Meal newMealTobeAdded){
    this.allMeals[mealsCount] = newMealTobeAdded;
    mealsCount +=1;
  }
  public void removeMealFromMenu(Meal mealTobeRemoved){

  }

  public Meal[] getAllMeals() {
    return allMeals;
  }

  public void setAllMeals(Meal[] allMeals) {
    this.allMeals = allMeals;
  }

  public String getMenuType() {
    return menuType;
  }

  public void setMenuType(String menuType) {
    this.menuType = menuType;
  }

  @Override
  public String toString() {
    return "Menu{" +
      "allMeals=" + Arrays.toString(allMeals) +
      ", menuType='" + menuType + '\'' +
      '}';
  }
}
