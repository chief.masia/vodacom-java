package za.co.vodacom.java.np.foodapp.updatedkitchen;

import java.util.ArrayList;

public class Menu {

  private String menuType;
  private ArrayList<Meal> allMeals;

  public Menu(String menuType) {
    this.menuType = menuType;
    this.allMeals = new ArrayList<>();
  }

  public void addMeal(Meal mealToAdd){
    allMeals.add(mealToAdd);
  }
  public void removeMeal(Meal mealToBeRemoved){
    allMeals.remove(mealToBeRemoved);
  }

  public void removeMealByName(String mealName){
    Meal mealToRemove = findMealByName(mealName);
    if(mealName != null) allMeals.remove(mealToRemove);
  }
  public Meal findMealByName(String mealName){
    for(Meal currentMeal:allMeals) {
      if(currentMeal.getName().equalsIgnoreCase(mealName)) return currentMeal;
    }
    return null;
  }


  @Override
  public String toString() {
    return "Menu{" +
      "menuType='" + menuType + '\'' +
      ", \n allMeals=" + allMeals +
      '}';
  }

  public ArrayList<Meal> getAllMeals() {
    return allMeals;
  }

  public void setAllMeals(ArrayList<Meal> allMeals) {
    this.allMeals = allMeals;
  }

  public String getMenuType() {
    return menuType;
  }

  public void setMenuType(String menuType) {
    this.menuType = menuType;
  }
}
