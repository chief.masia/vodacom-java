package za.co.vodacom.java.np.foodapp.updatedkitchen.order;

public interface IProduct {
  public String getName();
  public double getPrice();
  // public double getTotalPrice();
}
