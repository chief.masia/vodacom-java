package za.co.vodacom.java.np.foodapp.updatedkitchen.order;

import za.co.vodacom.java.np.foodapp.updatedkitchen.BevMeal;
import za.co.vodacom.java.np.foodapp.updatedkitchen.FoodMeal;
import za.co.vodacom.java.np.foodapp.updatedkitchen.Meal;
import za.co.vodacom.java.np.foodapp.updatedkitchen.Menu;

public class Main {
  public static void main(String[] args) {

//    Meal testMeal = new Meal("testmeal",10000.f);
//    FoodMeal testFoodMeal = new FoodMeal("testFoodMeal", 200000f);
//
//    Meal anotherMeal = new FoodMeal("AnotherFoodTestMeal", 1f);
//    FoodMeal anotherFoodMeal = new Meal("test",2f);

    Menu mealsMenu = initMenu();
    Order myOrder = new Order();
    FoodMeal pizza = (FoodMeal) mealsMenu.findMealByName("pizza");
    BevMeal orangeJuice = (BevMeal) mealsMenu.findMealByName("Orange Juice");
    BirthdayItem candles = new BirthdayItem("Candles", 50);

    myOrder.addToCart(pizza);
    myOrder.addToCart(orangeJuice);
    myOrder.addToCart(candles);

    System.out.println(myOrder.getTotalOrderPrice());
  }

  public static Menu initMenu(){
    FoodMeal pizzaMeal = new FoodMeal("Pizza", 43.2);
    FoodMeal pastaMeal = new FoodMeal("Pasta", 40.3);
    BevMeal orangeJuice = new BevMeal("Orange Juice", 18.3);
    BevMeal appleJuice = new BevMeal("Apple Juice", 15.9);

    Menu mealsMenu = new Menu("meals");
    mealsMenu.addMeal(pizzaMeal);
    mealsMenu.addMeal(pastaMeal);
    mealsMenu.addMeal(orangeJuice);
    mealsMenu.addMeal(appleJuice);

    return mealsMenu;
  }
}
