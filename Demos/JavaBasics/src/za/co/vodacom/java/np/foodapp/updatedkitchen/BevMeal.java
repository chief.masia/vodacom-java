package za.co.vodacom.java.np.foodapp.updatedkitchen;

public class BevMeal extends Meal{

  private static final double serviceCharge = 0.10;

  private float sizeInML;

  public float getSizeInML() {
    return sizeInML;
  }

  public void setSizeInML(float sizeInML) {
    this.sizeInML = sizeInML;
  }

  public BevMeal(String name, double price) {
    super(name, price);
  }

  @Override
  public String toString() {
    return "BevMeal{" +  super.toString()  +
      "sizeInML=" + sizeInML +
      '}';
  }
}
