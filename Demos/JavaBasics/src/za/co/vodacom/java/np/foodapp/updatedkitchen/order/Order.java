package za.co.vodacom.java.np.foodapp.updatedkitchen.order;
import za.co.vodacom.java.np.foodapp.updatedkitchen.Meal;
import java.util.ArrayList;
public class Order {
  private ArrayList<IProduct> orderItems;

  public Order(){
    this.orderItems = new ArrayList<>();
  }
  public void addToCart(IProduct product){
    orderItems.add(product);
  }
  public double getTotalOrderPrice(){
    double totalOrderPrice = 0;
    for (IProduct currentProduct: orderItems) {
      totalOrderPrice += currentProduct.getPrice();
      System.out.println("Current Item is: "+currentProduct.getName());
      System.out.println("Current Item Price is: "+currentProduct.getPrice());
    }
    return totalOrderPrice;
  }
}
