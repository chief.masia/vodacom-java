package za.co.vodacom.java.np.foodapp;
import za.co.vodacom.java.np.foodapp.kitchen.Meal;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Meal ahmedMeal = new Meal(21.7f, "Pizza");
        ahmedMeal.setPrice(-13.0f);
        ahmedMeal.setCalories(-100.0);
        System.out.println(ahmedMeal);

        Meal chefMeal = new Meal(15.3f, "Pap & Veils");
        chefMeal.setCalories(124.3);

        System.out.println(ahmedMeal);
        System.out.println(chefMeal);

        Meal saladMeal = new Meal();
        System.out.println(saladMeal);

    }
}
