package za.co.vodacom.java.np.foodapp.kitchen;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ModernMenu {

  private String menuType;
  private ArrayList<Meal> allMeals;

  public ModernMenu(String menuType) {
    this.menuType = menuType;
    this.allMeals = new ArrayList<>();
  }

  public void addAMealToMenu(Meal newMealToAdd){
    allMeals.add(newMealToAdd);
  }

  public void removeMealFromMenu(Meal mealToBeRemoved){
    allMeals.remove(mealToBeRemoved);
  }

  public ArrayList<Meal> getHealthMeals(){
    // cal is less than 20
    ArrayList<Meal> healthMeals = new ArrayList<>();

    return healthMeals;
  }
  public ArrayList<Meal> getCheapMeals(){
    // price is less than 20
    ArrayList<Meal> cheapMeals = new ArrayList<>();

    return cheapMeals;
  }
  public Meal findAMealByName(String mealThatIamLookingFor){
    for (int currentIndex = 0; currentIndex < allMeals.size(); currentIndex++) {
      Meal currentMeal = allMeals.get(currentIndex);
      String currentMealName = currentMeal.getName();
      if(currentMealName.equalsIgnoreCase(mealThatIamLookingFor)){
       return currentMeal;
      }
    }
    return null;
  }

  public void removeMealFromMenu(String mealNameToBeRemoved){
    Meal mealToBeRemoved = findAMealByName(mealNameToBeRemoved);
    if(mealToBeRemoved != null)  allMeals.remove(mealToBeRemoved);
  }

  public Meal getAMeal(int position){
    return allMeals.get(position);
  }
  public String getMenuType() {
    return menuType;
  }



  public void setMenuType(String menuType) {
    this.menuType = menuType;
  }

  @Override
  public String toString() {
    return "ModernMenu{" +
      "menuType='" + menuType + '\'' +
      ", allMeals=" + allMeals +
      '}';
  }
}
