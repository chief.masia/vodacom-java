package za.co.vodacom.java.np.foodapp.updatedkitchen;

import za.co.vodacom.java.np.foodapp.updatedkitchen.order.IProduct;

public class Meal implements IProduct {
  private String name;
  private double price;
  private double cal;
  private boolean veg;


  public Meal(String name, double price) {
    this.name = name;
    this.price = price;
  }


  @Override
  public String toString() {
    return "Meal{" +
      "name='" + name + '\'' +
      ", price=" + price +
      ", cal=" + cal +
      ", veg=" + veg +
      '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getCal() {
    return cal;
  }

  public void setCal(double cal) {
    this.cal = cal;
  }

  public boolean isVeg() {
    return veg;
  }

  public void setVeg(boolean veg) {
    this.veg = veg;
  }
}
