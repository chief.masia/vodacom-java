package za.co.vodacom.java.np.foodapp.updatedkitchen.order;

import za.co.vodacom.java.np.foodapp.updatedkitchen.Meal;

public class BirthdayItem  implements IProduct{
  private String name;
  private double price;
  private int quantity;

  public BirthdayItem(String name, float price) {
    this.name = name;
    this.price = price;
    this.quantity = 1;
  }

  @Override
  public String toString() {
    return "BirthdayItem{" +
      "name='" + name + '\'' +
      ", price=" + price +
      ", quantity=" + quantity +
      '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
}
