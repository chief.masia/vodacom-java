package za.vodacom.MealsAPI;

import org.springframework.aot.hint.annotation.Reflective;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class APIController {

  @GetMapping("/meal")
  public Meal getMeal(){
    Meal pizza = new Meal("Pizza", 21, 100,
      "https://files.elfsightcdn.com/b27fdf3d-b477-40ce-84d4-ddcade571fb4/f299acaf-76fe-4e14-aa67-fff28c5fdea0.jpeg");
    return pizza;
  }
  @GetMapping("/")
  public String home(){
    return "<h1>Welcome to my site</h1>";
  }
}
