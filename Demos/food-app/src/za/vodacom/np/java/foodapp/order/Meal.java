package za.vodacom.np.java.foodapp.order;

public class Meal implements IProduct{

    private String name;
    private double price;
    private double cal;
    private boolean isVegan;

    public Meal(String name, double price, double cal, boolean isVegan) {
        this.name = name;
        this.price = price;
        this.cal = cal;
        this.isVegan = isVegan;
    }

    public Meal(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", cal=" + cal +
                ", isVegan=" + isVegan +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getCal() {
        return cal;
    }

    public void setCal(double cal) {
        this.cal = cal;
    }

    public boolean isVegan() {
        return isVegan;
    }

    public void setVegan(boolean vegan) {
        isVegan = vegan;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
