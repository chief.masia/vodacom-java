package za.vodacom.np.java.foodapp.order;

public interface IProduct {

    public String getName();
    public double getPrice();

}
