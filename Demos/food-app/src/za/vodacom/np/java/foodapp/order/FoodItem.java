package za.vodacom.np.java.foodapp.order;

public class FoodItem extends Meal{

    public static final double VAT = 0.10;

    public FoodItem(String name, double price, double cal, boolean isVegan) {
        super(name, price, cal, isVegan);
    }

    public FoodItem(String name, double price) {
        super(name, price);
    }

    @Override
    public double getPrice() {
        return super.getPrice() * (1+FoodItem.VAT);
    }
}
