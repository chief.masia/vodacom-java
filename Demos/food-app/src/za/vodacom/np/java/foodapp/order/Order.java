package za.vodacom.np.java.foodapp.order;

import java.util.ArrayList;

public class Order {

    private ArrayList<IProduct> orderItems = new ArrayList<>();

    public void addItem(IProduct product) throws Exception {
        if (sizeHasReachedMaxLimit()) throw new Exception("Size Error, Item "+product.getName()+ " was not added");
        if (priceHasReachedTheLimit(product.getPrice())) throw new Exception("Price Error, "+product.getName()+ " was not added");
        orderItems.add(product);
    }

    public boolean sizeHasReachedMaxLimit(){
        return orderItems.size() >= 5;
    }

    public boolean priceHasReachedTheLimit(double priceOfNewItem){
        if(this.getTotalPrice()+priceOfNewItem <= 100){
            return false;
        }else {
            return true;
        }
    }
    public void removeAnItemByName(String itemName){

    }

    public double getTotalPrice(){
        double totalPrice = 0;
        for (IProduct item: orderItems) {
            totalPrice += item.getPrice();
        }
        return totalPrice;
    }


}
