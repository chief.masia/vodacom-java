package za.vodacom.np.java.foodapp.order;

public class BevItem extends Meal{

    public static final double VAT = 0.2;
    public BevItem(String name, double price, double cal, boolean isVegan) {
        super(name, price, cal, isVegan);
    }

    public BevItem(String name, double price) {
        super(name, price);
    }

    @Override
    public double getPrice() {
        return super.getPrice()*(1+BevItem.VAT);
    }
}
