package za.vodacom.java.apis.FoodAppAPI;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class MealsAPIController {

  private Menu mainMenu = new Menu();
  @GetMapping("/")
  public String home(){
    return "Welcome to our App";
  }


  @GetMapping("/meals")
  public ArrayList<Meal> getAllMeals(){
    if(this.mainMenu.getMainMenu().size() == 0) initMenu();
    return mainMenu.getMainMenu();
  }

  @GetMapping("/mealbyname")
  public Meal getAMealByName(String name){
    return this.mainMenu.getAMealByName(name);
  }



  @GetMapping("/meal")
  public Meal getAMeal(int index){
    return this.mainMenu.getAMealByIndex(index);
  }
  @GetMapping("/hello")
  public String hello(String name){
    return "Hello, "+name;
  }


  private void initMenu(){
    Meal pizza = new Meal("Pizza", 50, "");
    Meal pasta = new Meal("pasta", 30, "");
    Meal fries = new Meal("fries", 10, "");
    this.mainMenu.addAMeal(pizza);
    this.mainMenu.addAMeal(pasta);
    this.mainMenu.addAMeal(fries);
  }


  @GetMapping("/cheapmeals")
  public ArrayList<Meal> getCheapMaels(){
    return this.mainMenu.getCheapMeals();
  }

  @GetMapping("/addMeal")
  public String addAMeal(String name, int price, String url){
    Meal newMeal = new Meal(name, price, url);
    this.mainMenu.addAMeal(newMeal);
    return "Meal has been successfully added";
  }



}
