package za.vodacom.java.apis.FoodAppAPI;

public class Meal {
  private String name;
  private int price;
  private String imageURL;

  public Meal(String name, int price, String imageURL) {
    this.name = name;
    this.price = price;
    this.imageURL = imageURL;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getImageURL() {
    return imageURL;
  }

  public void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }
}
