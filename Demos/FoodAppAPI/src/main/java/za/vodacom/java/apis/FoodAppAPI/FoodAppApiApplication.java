package za.vodacom.java.apis.FoodAppAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodAppApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodAppApiApplication.class, args);
	}

}
