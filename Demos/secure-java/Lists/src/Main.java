import com.nobleprog.java.sec.lists.Employee;
import com.nobleprog.java.sec.lists.HRDept;
import com.nobleprog.java.sec.UserInterface;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
  public static void main(String[] args) {
    HRDept hrDept = new HRDept();
    Employee empOne = new Employee("Emp One", "emp.one@company.com");
    Employee empTwo = new Employee("Emp Two", "emp.two@company.com");
    Employee empThree = new Employee("Emp Three", "emp.three@company.com");
    hrDept.hire(empOne);
    hrDept.hire(empTwo);
    hrDept.hire(empThree);

    UserInterface reader = new UserInterface();

    while(true){
      String command = reader.getStringValueFromUser("Enter a command (New, Search, Exit or help)");
      if(command.equalsIgnoreCase("new")){
        int position = reader.getIntValueFromUser("Enter New Employee Position");
        String name = reader.getStringValueFromUser("Enter New Employee Name");
        String email = reader.getStringValueFromUser("Enter New Employee Email");
        try {
          hrDept.hire(new Employee(name, email), position);
        } catch (Exception e) {
          System.out.println(e.getMessage());
        }
      }
      else if(command.equalsIgnoreCase("search")){
        int empIndex = reader.getIntValueFromUser("Enter Employee Number or Enter -1 to exit");
        if(empIndex == -1) break;
        Employee mentionedEmployee = null;
        try {
          mentionedEmployee = hrDept.getEmployeeAtIndex(empIndex);
        } catch (Exception e) {
          System.out.println("Employee you have asked for, does not exist");
          continue;
        }
        System.out.println(mentionedEmployee.getName());
      }
      else if (command.equalsIgnoreCase("exit")){
        System.out.println("Thanks for using our program");
        break;
      }
      else if(command.equalsIgnoreCase("help")){
        System.out.println("Use command new to create a new user");
        System.out.println("Use command search to search for an existing user");
        System.out.println("Use command exit to exit the program");
        System.out.println("Use command help to show this help screen");
      }
    }
  }

}
