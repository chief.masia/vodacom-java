package com.nobleprog.java.sec;

import java.util.Scanner;

public class UserInterface {

    public int getIntValueFromUser(String tagLine){
        System.out.println(tagLine);
        Scanner inputReader = new Scanner(System.in);
        return inputReader.nextInt();
    }
    public String getStringValueFromUser(String tagLine){
        System.out.println(tagLine);
        Scanner inputReader = new Scanner(System.in);
        return inputReader.nextLine();
    }


}
