package com.nobleprog.java.sec.network;

import com.nobleprog.java.sec.UserInterface;

public class Main {

    public static void main(String[] args) {
        PingController ping = new PingController();
        ping.start();
    }
}
