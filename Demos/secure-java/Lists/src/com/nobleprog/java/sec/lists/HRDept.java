package com.nobleprog.java.sec.lists;

import java.util.ArrayList;

public class HRDept {
  private ArrayList<Employee> allEmployees;

  public HRDept() {
    this.allEmployees = new ArrayList<>();
  }
  public void hire(Employee newEmployee){
    allEmployees.add(newEmployee);
  }

  public void hire(Employee newEmployee, int position) throws Exception {
    // check position - if valid add - else throw an exception
    if(position > allEmployees.size()){
      throw new Exception("Out of bounds write");
    }
    allEmployees.add(position, newEmployee);
  }
  public Employee getEmployeeAtIndex(int index) throws Exception{
    if(index < allEmployees.size()){
      return allEmployees.get(index);
    }
    else{
      throw new Exception("Employee is not found!");
    }
  }
}
