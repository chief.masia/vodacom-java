package com.nobleprog.java.sec.files;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class UserInterface {
    public void mainUserInterface(){
        while (true){
            String userInput = getStringValueFromUser("You can read, write or exit");
            if(userInput.equalsIgnoreCase("read")) {
                handleReadFileCase();
            } else if (userInput.equalsIgnoreCase("write")) {
                handleWriteCase();
            }else if(userInput.equalsIgnoreCase("exit")){
                System.out.println("Thanks for using our secure program");
                break;
            }else{
                System.out.println("Unknown option, try again");
            }
        }
    }

    private void handleWriteCase() {
        String fileName = getStringValueFromUser("Enter file name");
        String userData = getStringValueFromUser("Enter data");
        FilesManager mrTshif = new FilesManager();
        try {
            mrTshif.writeToFile(fileName, userData);
        } catch (IOException e) {
            System.out.println("Couldn't write to the file");
        }
    }

    private void handleReadFileCase() {
        String fileName = getStringValueFromUser("Enter file name");
        FilesManager mrTshif = new FilesManager();
        String fileContents = null;
        try {
            fileContents = mrTshif.fileRead(fileName);
            System.out.println(fileContents);
        } catch (FileNotFoundException e) {
            System.out.println("The file you have entered doesn't exist");
        } catch (IOException e){
            System.out.println("Can't read from file");
        }

    }

    public int getIntValueFromUser(String tagLine){
        System.out.println(tagLine);
        Scanner inputReader = new Scanner(System.in);
        return inputReader.nextInt();
    }
    public String getStringValueFromUser(String tagLine){
        System.out.println(tagLine);
        Scanner inputReader = new Scanner(System.in);
        return inputReader.nextLine();
    }
}
