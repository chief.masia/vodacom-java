package com.nobleprog.java.sec.network;

import com.nobleprog.java.sec.UserInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PingController {
    public void start(){
        while(true){
            UserInterface userInterface = new UserInterface();
            String userCommand = userInterface.getStringValueFromUser("Enter the command or enter exit");
            if(userCommand.equalsIgnoreCase("ping")){
                handlePingRequest();
            } else if (userCommand.equalsIgnoreCase("exit")) {
                break;
            }else {
                System.out.println("Unknown Option, try again");
            }
        }
    }

    private void handlePingRequest() {
        String ip = readIPAddress();
        try {
            runPing(ip);
        } catch (IOException e) {
            System.out.println("Couldn't run command");;
        } catch (InterruptedException e) {
            System.out.println("Command was interrupted");;
        }
    }

    private void runPing(String ip) throws IOException, InterruptedException {
        runCommand("ping "+ ip);
    }

    private void runCommand(String command) throws IOException, InterruptedException {
        Runtime shell = Runtime.getRuntime();
        Process pingProcess = shell.exec(command);
        pingProcess.waitFor();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(pingProcess.getInputStream()));
        String line= "";
        while((line = buffer.readLine()) != null){
            System.out.println(line);
        }
        buffer.close();
    }


    private String readIPAddress() {
        UserInterface ui = new UserInterface();
        String ip =
                ui.getStringValueFromUser("Enter the hostname or the ip address");
        return ip;
    }
}
