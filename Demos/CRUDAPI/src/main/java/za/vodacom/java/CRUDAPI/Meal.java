package za.vodacom.java.CRUDAPI;

import java.util.UUID;

public class Meal {
  private UUID id;
  private String name;
  private int price;
  private String imageUrl;

  public Meal(String name, int price, String url) {
    this.name = name;
    this.price = price;
    this.imageUrl = url;
    this.id = UUID.randomUUID();
  }

  @Override
  public String toString() {
    return "Meal{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", price=" + price +
            ", imageUrl='" + imageUrl + '\'' +
            '}';
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }
}
