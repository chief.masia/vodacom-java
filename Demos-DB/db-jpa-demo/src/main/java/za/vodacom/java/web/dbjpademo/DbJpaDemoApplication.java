package za.vodacom.java.web.dbjpademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbJpaDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbJpaDemoApplication.class, args);
	}

}
