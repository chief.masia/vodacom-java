package za.vodacom.java.web.dbjpademo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.vodacom.java.web.dbjpademo.entities.Product;
import za.vodacom.java.web.dbjpademo.repo.ProductRepo;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private final ProductRepo repo;
    public ProductsController(ProductRepo repo){
        this.repo = repo;
    }
    @GetMapping
    public List<Product> getAllProducts(){
        return  repo.findAll();
    }
    @GetMapping("/{id}")
    public Product getAProductById(@PathVariable Long id){
        return repo.findById(id).orElse(null);
    }
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id){
        repo.deleteById(id);
    }
    @PutMapping("/{id}")
    public Product updateAProduct(@PathVariable Long id, @RequestBody Product updatedProduct){
        if(repo.existsById(id)){
            updatedProduct.setId(id);
            return repo.save(updatedProduct);
        }
        else {
            return null;
        }
    }
    @PostMapping
    public Product addProduct(@RequestBody Product newProduct){
        return repo.save(newProduct);
    }

}
