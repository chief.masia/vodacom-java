package za.vodacom.java.web.dbjpademo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import za.vodacom.java.web.dbjpademo.entities.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {

}
