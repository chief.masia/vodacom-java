package com.vodacom.java.web.apidbdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDbDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiDbDemoApplication.class, args);
	}

}
