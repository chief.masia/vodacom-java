## Java Items

- Basic Syntax (Variables, Loops, Conditions)
- OOP Basics (Classes, Objects, Static, Constructors, Setters and Getters, toSting)
- OOP Concepts (Inheritance, Polymorphism, Interfacing, Encapsulation)
- Exception Handling 
- Working with Files and Serailization 
- HTTP Connections and JSON
- Java APIs
